module.exports = function(grunt) {
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

          imagemin: {
              dynamic: {
                  files: [{
                      expand: true,
                      cwd: 'src/assets/img/',
                      src: ['**/*.jpg'],
                      dest: 'dist/assets/img/'
                  }]
              }
          },

          htmlmin: {                                     
            dist: {                                      
              options: {                                 
                removeComments: true,
                collapseWhitespace: true
              },
              files: [{
                  expand: true,
                  cwd: 'src',
                  src: ['*.html'],
                  dest: 'dist/'
              }]
            }
          },

          sass: {
            dist: {
              files: {
                'dist/styles/all_styles.css' : 'dist/styles/scss/all_styles.scss'
              }                 
            }
          },

          concat: {
            dist: {
              src: ['src/styles/scss/*.scss'],
              dest: 'dist/styles/scss/all_styles.scss',
            },
          },

          cssmin: {                                    
            target: {
              files: {
                'dist/styles/styles.css' : 'dist/styles/all_styles.css'
              }             
            }
          },

          watch: {

              options: {
                livereload: true,
              },

              html: {
                  files: ['src/*.html'],
                  tasks: ['htmlmin'],
              },

              images: {
                  files: ['src/assets/img/**/*.jpg'],
                  tasks: ['imagemin'],
              },

              concat: {
                  files: ['src/styles/scss/*.scss'],
                  tasks: ['concat'],
              },

              sass: {
                  files: ['dist/styles/scss/all_styles.scss'],
                  tasks: ['sass'],
              },

              css: {
                  files: ['dist/styles/all_styles.css'],
                  tasks: ['cssmin'],
              }              
          } 
    });

grunt.loadNpmTasks('grunt-contrib-imagemin');
grunt.loadNpmTasks('grunt-contrib-htmlmin');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-sass');

//grunt.registerTask('default', ['sass']);
grunt.registerTask('default', ['htmlmin', 'imagemin', 'concat', 'sass', 'cssmin', 'watch']);

};
